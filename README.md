->Node.js Form Data Submission
This project demonstrates a simple Node.js application that allows users to submit form data and stores it in a text file.

->Prerequisites:

Node.js and npm installed
Running the application:
Run npm install to install dependencies.
Start the server by running "node process.js"
The application will be available at http://localhost:2000

->Functionality:
The application provides a web interface with a form where users can enter their First Name, Last Name, and Email Address. Upon submitting the form, the data is:
Extracted from the request body.
Appended to a text file named data.txt located in the project root directory.

->CI/CD implementation
Create a .gitlab-ci.yml file and create the pipeline
Pipeline Stages

build_stage:

Purpose: Builds the application and prepares it for deployment.
Image: Uses a node image to provide a Node.js environment.
Script:
npm install: Installs the application's dependencies.
Artifacts:
node_modules: Captures the installed dependencies for deployment.
package-lock.json: Ensures consistent dependency versions.
deploy_stage:

Purpose: Deploys the built application to the target environment.
Image: Uses the same node image for consistency.
Script:
node process.js /dev/null 2>&1 &: Starts the application server in the background.